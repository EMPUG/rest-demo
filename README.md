## RESETful API demo

This application uses the Flask (the Python web framework) and SQLAlchemy
to demonstrate a small RESTful API. It uses a small SQLite database, but other
databases, such as PostgreSQL or MySQL could be used instead.

The following commands work on Mac OS and Linux; adjustments will have to be
made for Windows.

### Installation

1. Create a directory somewhere for this project and make it the active directory:

        mkdir rest-demo
        cd rest-demo

2. Create a Python 3 virtual environment called `env`:

        python3 -m venv env
        
3. Activate the environment:

        source env/bin/activate
        
4. Clone the git repository with the application into a new directory called `demo`:

        git clone git@github.com:empug/rest-demo.git demo

5. Navigate to the `demo` directory:

        cd demo

6. Install required packages:

        pip install -r requirements.txt

7. Create the SQLite database:

        python create_db.py
        
8. Add sample data to the database:

        python add_sample_data.py

### Running the demo

1. Activate the virtual environment, if not already active:

        cd rest-demo
        source env/bin/activate
        
2. Navigate to the demo directory:

        cd demo
        
2. Launch the Flask application in development mode:

        export FLASK_ENV=development
        export FLASK_APP=demo
        flask run
