"""
RESTful API demo application.
"""

import json

from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)

# Database model.

class Member(db.Model):
    """
    Represents people who are members of the organization.
    """
    __tablename__ = 'member'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    active = db.Column(db.Boolean, default=True)
    phones = db.relationship('Phone', cascade='all,delete', backref='member')


class Phone(db.Model):
    """
    Represents members' phones.
    """
    __tablename__ = 'phone'

    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer, db.ForeignKey('member.id'))
    phone_number = db.Column(db.String(50))
    phone_type = db.Column(db.String(50))

# Validation.

def validate_member_data(data):
    """
    Validates member data. firstName and lastName are required. If present,
    active must be boolean.
    """
    errors = []
    if not 'firstName' in data:
        errors.append('firstName is required')
    if not 'lastName' in data:
        errors.append('lastName is required')
    if 'active' in data and not isinstance(data['active'], bool):
        errors.append(('active must be boolean'))
    return errors


# Routes.

@app.route('/api/v1/health-check')
def health_check():
    return jsonify({'status': 'OK'})


@app.route('/api/v1/members', methods=['GET', 'POST'])
def members_route():
    if request.method == 'GET':
        # Return the list of members.
        members = Member.query.order_by(Member.last_name, Member.first_name).all()
        results = {'data': []}
        for member in members:
            results['data'].append({
                'id': member.id,
                'firstName': member.first_name,
                'lastName': member.last_name,
                'active': member.active,
            })
        return jsonify(results)
    elif request.method == 'POST':
        # Validate the JSON request.
        errors = validate_member_data(request.json)
        if errors:
            return jsonify({'errors': errors}), 422

        # Read the JSON properties.
        first_name = request.json['firstName']
        last_name = request.json['lastName']
        if 'active' in request.json:
            active = request.json['active']
        else:
            active = True

        # Make sure member does not already exist.
        existing_member = Member.query.filter_by(first_name=first_name).filter_by(last_name=last_name).first()
        if existing_member:
            return jsonify({'error': 'Member already exists'}), 422

        # Add the new member and return the Id.
        member = Member()
        member.first_name = first_name
        member.last_name = last_name
        member.active = active
        db.session.add(member)
        db.session.commit()
        return jsonify({'id': member.id}), 201


@app.route('/api/v2/members')
def members_route_with_phones():
    members = Member.query.order_by(Member.last_name, Member.first_name).all()
    results = {'data': []}
    for member in members:
        member_phones = []
        for phone in member.phones:
            member_phones.append({
                'phoneNumber': phone.phone_number,
                'phoneType': phone.phone_type
            })
        results['data'].append({
            'id': member.id,
            'firstName': member.first_name,
            'lastName': member.last_name,
            'active': member.active,
            'phones': member_phones
        })
    return jsonify(results)


@app.route('/api/v1/members/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def member_route(id):
    # Make sure the member exists.
    member = Member.query.get(id)
    if not member:
        return jsonify({'error': 'Member does not exist'}), 422

    if request.method == 'GET':
        # Return the information about the member.
        results = {'data': [{
            'id': member.id,
            'firstName': member.first_name,
            'lastName': member.last_name,
            'active': member.active,
        }]}
        return jsonify(results)
    elif request.method == 'PUT':
        # Validate the JSON request.
        errors = validate_member_data(request.json)
        if errors:
            return jsonify({'errors': errors}), 422

        # Read the JSON properties.
        first_name = request.json['firstName']
        last_name = request.json['lastName']
        if 'active' in request.json:
            active = request.json['active']
        else:
            active = member.active

        # Update the member record.
        member.first_name = first_name
        member.last_name = last_name
        member.active = active
        db.session.commit()
        return jsonify({'status': 'success'})
    elif request.method == 'DELETE':
        db.session.delete(member)
        db.session.commit()
        return jsonify({'status': 'success'})


@app.route('/api/v1/members/<int:id>/phones')
def member_phones(id):
    # Make sure the member exists.
    member = Member.query.get(id)
    if not member:
        return jsonify({'error': 'Member does not exist'}), 422

    results = {'data': []}
    for phone in member.phones:
        results['data'].append({
            'id': phone.id,
            'phoneNumber': phone.phone_number,
            'phoneType': phone.phone_type
        })
    return jsonify(results)


# Error handlers.

@app.errorhandler(400)
def method_not_allowed(e):
    return jsonify({'error': str(e)}), 400


@app.errorhandler(404)
def page_not_found(e):
    return jsonify({'error': 'Resource not found'}), 404


@app.errorhandler(405)
def method_not_allowed(e):
    return jsonify({'error': 'Method not allowed'}), 405


@app.errorhandler(500)
def internal_server_error(e):
    return jsonify({'error': '500 - internal server error'}), 500
