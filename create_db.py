"""
Create the RESTful API demo database.
"""

from demo import db

# Create the database based on the model.
db.create_all()
