"""
Add sample people and phones to the RESTful API demo database.
"""

from demo import db
from demo import Member, Phone

# Delete records from tables of interest.
Member.query.delete()
Phone.query.delete()

# John Adams

member = Member()
member.first_name = 'John'
member.last_name = 'Adams'

phone = Phone()
phone.phone_number = '555-1234'
phone.phone_type = 'home'
member.phones.append(phone)

phone = Phone()
phone.phone_number = '555-5555'
phone.phone_type = 'cell'
member.phones.append(phone)

db.session.add(member)

# Sara Smith

member = Member()
member.first_name = 'Sara'
member.last_name = 'Smith'

phone = Phone()
phone.phone_number = '555-9999'
phone.phone_type = 'cell'
member.phones.append(phone)

db.session.add(member)

# Richard Recluse

member = Member()
member.first_name = 'Richard'
member.last_name = 'Recluse'
member.active = False

db.session.add(member)

# Commit the records to the database.
db.session.commit()
